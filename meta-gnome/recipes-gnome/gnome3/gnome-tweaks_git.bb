DESCRIPTION = "GNOME Tweaks"
LICENSE = "GPL-3.0"
DEPENDS = " \
    gnome-shell \
"

LIC_FILES_CHKSUM = " \
    file://LICENSES/GPL-3.0;md5=9eef91148a9b14ec7f9df333daebc746 \
    file://LICENSES/CC0-1.0;md5=65d3616852dbf7b1a6d4b53b00626032 \
"

SRC_URI = " \
    git://gitlab.gnome.org/GNOME/gnome-tweaks.git;protocol=http;branch=master \
"

SRCREV = "ed6b6d5b509f0f600ea4d1150e79ba3e2fde84c7"

S = "${WORKDIR}/git"

inherit pkgconfig meson gobject-introspection python3-dir

FILES_${PN} += " \
    ${datadir} \
    ${PYTHON_SITEPACKAGES_DIR} \
"

